﻿<%@ Page Title="" Language="C#" MasterPageFile="~/welcome/userlogged.master" AutoEventWireup="true" CodeFile="profile.aspx.cs" Inherits="userlogged_profile" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <p>
        <br />
    </p>
    <p style="width: 252px">
        <asp:Image ID="imgprofile" runat="server" Width="250px"/>
        <asp:FileUpload ID="fluprofile" runat="server" Width="100px" ForeColor="White" 
            Height="30px"/>
        <asp:RoundedCornersExtender ID="fluprofile_RoundedCornersExtender" 
            runat="server" Enabled="True" Radius="2" TargetControlID="fluprofile">
        </asp:RoundedCornersExtender>
        <asp:Button ID="btnupload" runat="server" Text="Upload" 
            onclick="btnupload_Click" />
        <asp:RoundedCornersExtender ID="btnupload_RoundedCornersExtender1" 
            runat="server" Enabled="True" Radius="2" TargetControlID="btnupload">
        </asp:RoundedCornersExtender>
    </p>
    <p>
        <asp:HyperLink ID="hplfirstname" runat="server" ForeColor="White" 
            NavigateUrl="~/userlogged/logged1.aspx" Font-Bold="True" 
            Font-Size="Large" BorderColor="White" BorderStyle="Inset" 
            BorderWidth="6px" Width="170px" >[hplfirstname]</asp:HyperLink>
        &nbsp;</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
    <p>
    style="width: 1109px">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblupdateprofile" runat="server" Text="Profile View" 
            ForeColor="White" Width="300px" BackColor="Black" BorderColor="White" 
            BorderStyle="Double" BorderWidth="5px" Font-Bold="True" Font-Overline="False" 
            Font-Size="XX-Large" Font-Strikeout="False" Font-Underline="False"></asp:Label>
        &nbsp;
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblinfo" runat="server" Text="Personal Information" 
            ForeColor="White" Width="300px" BorderColor="White" BorderStyle="Double" 
            BorderWidth="5px" Height="30px" Font-Bold="True" Font-Size="X-Large"></asp:Label>
    </p>
     <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblfname" runat="server" Text="First Name" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtfirstname" runat="server" Width="300px" Height="25px" 
             ForeColor="White"></asp:Label>
        
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lbllname" runat="server" Text="Last Name" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtlastname" runat="server" Width="300px" Height="25px" 
            ForeColor="White"></asp:Label>
        
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lbldisplayname" runat="server" Text="Display Name" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtdisplayname" runat="server" Width="300px" Height="25px" 
            ForeColor="White"></asp:Label>
        
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lbldob" runat="server" Text="Date of Birth" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtdob" runat="server" Width="300px" Height="25px" 
            ForeColor="White" ></asp:Label>
       
    </p>
 <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblpadd" runat="server" Text="Personal Address" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtpadd" runat="server" Width="300px" 
            TextMode="MultiLine" Height="40px" ForeColor="White"></asp:Label>
     
    </p>
        <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblpmobile" runat="server" Text="Personal Mobile Number" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtmobileno" runat="server" Width="300px" Height="25px" 
                ForeColor="White"></asp:Label>
           
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblhobbies" runat="server" Text="Hobbies" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txthobbies" runat="server" Width="300px" Height="25px" 
            ForeColor="White"></asp:Label>
        
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lbllikes" runat="server" Text="Likes" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtlikes" runat="server" Width="300px" Height="25px" 
            ForeColor="White"></asp:Label>
       
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lbldislikes" runat="server" Text="Dislikes" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtdislikes" runat="server" Width="300px" Height="25px" 
            ForeColor="White"></asp:Label>
        
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblabtu" runat="server" Text="About You" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtaboutu" runat="server" Width="300px" Height="25px" 
            TextMode="MultiLine" ForeColor="White"></asp:Label>
       
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </p>
    <p>
    </p>
    <p>
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lbleducation" runat="server" Text="Education" 
            ForeColor="White" Width="300px" BorderColor="White" BorderStyle="Double" 
            BorderWidth="5px" Height="30px" Font-Bold="True" Font-Size="X-Large"></asp:Label>
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblcurrent" runat="server" Text="Current Status" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:RadioButton ID="rdbstudent" runat="server" ForeColor="White" 
            GroupName="edu" Text="Student"/>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:RadioButton ID="rdbworking" runat="server" ForeColor="White" 
            GroupName="edu" Text="Working"/>
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblhighschool" runat="server" Text="High School" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txthighschool" runat="server" Width="300px" Height="25px" 
            ForeColor="White"></asp:Label>
       
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblinterclg" runat="server" Text="Inter College" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtinterclg" runat="server" Width="300px" Height="25px" 
            ForeColor="White"></asp:Label>
        
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblgraduation" runat="server" Text="Graduation" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtgraduation" runat="server" Width="300px" Height="25px" 
            ForeColor="White"></asp:Label>
        
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblpostgrad" runat="server" Text="Post Graduation" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtpostgrad" runat="server" Width="300px" Height="25px" 
            ForeColor="White"></asp:Label>
       
    </p>
    <p>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </p>
    <p>
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblwrkinfo" runat="server" Text="Work Information" 
            ForeColor="White" Width="300px" BorderColor="White" BorderStyle="Double" 
            BorderWidth="5px" Height="30px" Font-Bold="True" Font-Size="X-Large"></asp:Label>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblprofession" runat="server" Text="Profession" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtprofession" runat="server" Width="300px" Height="25px" 
            ForeColor="White"></asp:Label>
        
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblcname" runat="server" Text="Company Name" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtcname" runat="server" Width="300px" Height="25px" 
            ForeColor="White"></asp:Label>
        
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblcadd" runat="server" Text="Company Address" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtcadd" runat="server" Width="300px" TextMode="MultiLine" 
            Height="40px" ForeColor="White"></asp:Label>

    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblcompanymobile" runat="server" Text="Office Mobile Number" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtcmobile" runat="server" Width="300px" Height="25px" 
            ForeColor="White"></asp:Label>
            
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblcemail" runat="server" Text="Company Email ID" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="txtcemail" runat="server" Width="300px" Height="25px" 
            ForeColor="White"></asp:Label>
       
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
</asp:Content>

