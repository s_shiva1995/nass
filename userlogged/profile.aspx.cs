﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class userlogged_profile : System.Web.UI.Page
{
    SqlConnection cn;
    SqlCommand cmd;
    SqlDataReader rd;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int i = Convert.ToInt32(Session["userid"]);
            string username = Session["username"].ToString();
            int j = Convert.ToInt32(Session["i"]);
            if (j == 1)
            {
                hplfirstname.Text = Session["firstname"].ToString() + " " + Session["lastname"].ToString();
            }
            if (j == 2)
            {
                hplfirstname.Text = Session["displayname"].ToString();
            }
            imgprofile.ImageUrl = Session["profileimage"].ToString();
        }
        catch
        {
            Response.Redirect("~//login//login.aspx");
        }
        cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Y510\Documents\Visual Studio 2010\WebSites\NASS\App_Data\user_profile.mdf;Integrated Security=True;User Instance=True;");
        string selectpersonalinfo = "Select * from personalinfo where (userid='" + Session["userid"].ToString() + "')";
        string selecteducation = "Select * from education where (userid='" + Session["userid"].ToString() + "')";
        string selectworkinfo = "Select * from workinfo where (userid='" + Session["userid"].ToString() + "')";
        if (!IsPostBack)
        {
            cmd = new SqlCommand(selectpersonalinfo, cn);
            cn.Open();
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                txtfirstname.Text = rd["firstname"].ToString();
                txtlastname.Text = rd["lastname"].ToString();
                if (rd["displayname"].ToString() == null || rd["displayname"].ToString() == "")
                {
                    txtdisplayname.Text = rd["firstname"].ToString() + " " + rd["lastname"].ToString();
                }
                else
                {
                    txtdisplayname.Text = rd["displayname"].ToString();
                }
                txtdob.Text = rd["dateofbirth"].ToString();
                txtpadd.Text = rd["personaladdress"].ToString();
                txtmobileno.Text = rd["personalmobileno"].ToString();
                txthobbies.Text = rd["hobbies"].ToString();
                txtlikes.Text = rd["likes"].ToString();
                txtdislikes.Text = rd["dislikes"].ToString();
                txtaboutu.Text = rd["aboutu"].ToString();
            }
            rd.Close();
            cmd = new SqlCommand(selecteducation, cn);
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                int i = 0;
                ViewState["i"] = i.ToString();
                if (rd["currentstatus"].ToString() == "Student")
                {
                    rdbstudent.Checked = true;
                    i++;
                    ViewState["i"] = i.ToString();
                }
                if (rd["currentstatus"].ToString() == "Working")
                {
                    i++;
                    rdbworking.Checked = true;
                    ViewState["i"] = i.ToString();
                }
                i = Convert.ToInt32(ViewState["i"]);
                if (i == 0)
                {
                    rdbstudent.Checked = false;
                    rdbworking.Checked = false;
                }
                txthighschool.Text = rd["highschool"].ToString();
                txtinterclg.Text = rd["intercollege"].ToString();
                txtgraduation.Text = rd["graduation"].ToString();
                txtpostgrad.Text = rd["postgraduation"].ToString();
            }
            rd.Close();
            cmd = new SqlCommand(selectworkinfo, cn);
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                txtprofession.Text = rd["profession"].ToString();
                txtcname.Text = rd["companyname"].ToString();
                txtcadd.Text = rd["companyaddress"].ToString();
                txtcmobile.Text = rd["officemob"].ToString();
                txtcemail.Text = rd["companyemail"].ToString();
            }
        }
        cn.Close();
    }
    protected void btnupload_Click(object sender, EventArgs e)
    {
        string updateimage = "update detail set profileimage='" + fluprofile.FileName + "' where userid='" + Session["userid"].ToString() + "'";
        cmd = new SqlCommand(updateimage, cn);
        cn.Open();
        cmd.ExecuteReader();
        cn.Close();
    }

}