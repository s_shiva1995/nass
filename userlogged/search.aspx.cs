﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class userlogged_search : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int i = Convert.ToInt32(Session["userid"]);
            string username = Session["username"].ToString();
            int j = Convert.ToInt32(Session["i"]);
            if (j == 1)
            {
                hplfirstname.Text = Session["firstname"].ToString() + " " + Session["lastname"].ToString();
            }
            if (j == 2)
            {
                hplfirstname.Text = Session["displayname"].ToString();
            }
            imgprofile.ImageUrl =Session["profileimage"].ToString();
        }
        catch
        {
            Response.Redirect("~//login//login.aspx");
        }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        string username = ((Label)GridView1.Rows[GridView1.SelectedIndex].FindControl("lblusername")).Text;
        Label3.Text = username;
        Session["friendusername"] = username;
        Response.Redirect("~//friendview//homefriend.aspx");
    }
}