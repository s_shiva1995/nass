﻿<%@ Page Title="" Language="C#" MasterPageFile="~/welcome/userlogged.master" AutoEventWireup="true" CodeFile="updateprofile.aspx.cs" Inherits="userlogged_updateprofile" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <p>
        <br />
    </p>
    <p>
        <asp:Image ID="imgprofile" runat="server" Width="250px"/>
    </p>
    <p>
        <asp:HyperLink ID="hplfirstname" runat="server" ForeColor="White" 
            NavigateUrl="~/userlogged/logged1.aspx" Font-Bold="True" 
            Font-Size="Large" BorderColor="White" BorderStyle="Inset" 
            BorderWidth="6px" Width="170px" >[hplfirstname]</asp:HyperLink>
        &nbsp;</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <p>
        &nbsp;</p>
    <p style="width: 1109px">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblupdateprofile" runat="server" Text="Update Profile" 
            ForeColor="White" Width="300px" BackColor="Black" BorderColor="White" 
            BorderStyle="Double" BorderWidth="5px" Font-Bold="True" Font-Overline="False" 
            Font-Size="XX-Large" Font-Strikeout="False" Font-Underline="False"></asp:Label>
        &nbsp;</p>
    <p>
    </p>
    <p>
    </p>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblinfo" runat="server" Text="Personal Information" 
            ForeColor="White" Width="300px" BorderColor="White" BorderStyle="Double" 
            BorderWidth="5px" Height="30px" Font-Bold="True" Font-Size="X-Large"></asp:Label>
    </p>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblfname" runat="server" Text="First Name" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtfirstname" runat="server" Width="300px" Height="25px"></asp:TextBox>
        <asp:RoundedCornersExtender ID="txtfirstname_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="txtfirstname">
        </asp:RoundedCornersExtender>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lbllname" runat="server" Text="Last Name" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtlastname" runat="server" Width="300px" Height="25px"></asp:TextBox>
        <asp:RoundedCornersExtender ID="txtlastname_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="txtlastname">
        </asp:RoundedCornersExtender>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lbldisplayname" runat="server" Text="Display Name" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtdisplayname" runat="server" Width="300px" Height="25px"></asp:TextBox>
        <asp:RoundedCornersExtender ID="txtdisplayname_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="txtdisplayname">
        </asp:RoundedCornersExtender>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lbldob" runat="server" Text="Date of Birth" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtdob" runat="server" Width="300px" Height="25px" ></asp:TextBox>
        <asp:TextBoxWatermarkExtender ID="txtdob_TextBoxWatermarkExtender" 
            runat="server" Enabled="True" TargetControlID="txtdob" 
            WatermarkText="dd/mm/yyyy">
        </asp:TextBoxWatermarkExtender>
        <asp:CalendarExtender ID="txtdob_CalendarExtender" runat="server" 
            DaysModeTitleFormat="d MMMM, yyyy" Enabled="True" Format="d MMMM, yyyy" 
            PopupPosition="Right" TargetControlID="txtdob" TodaysDateFormat="d MMMM , yyyy">
        </asp:CalendarExtender>
        <asp:RoundedCornersExtender ID="txtdob_RoundedCornersExtender" runat="server" 
            Enabled="True" TargetControlID="txtdob">
        </asp:RoundedCornersExtender>
    </p>
 <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblpadd" runat="server" Text="Personal Address" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtpadd" runat="server" Width="300px" 
            TextMode="MultiLine" Height="40px"></asp:TextBox>
     <asp:DropDownExtender ID="TextBox1_DropDownExtender" runat="server" 
         DynamicServicePath="" Enabled="True" TargetControlID="txtpadd">
     </asp:DropDownExtender>
     <asp:RoundedCornersExtender ID="TextBox1_RoundedCornersExtender" runat="server" 
         Enabled="True" TargetControlID="txtpadd">
     </asp:RoundedCornersExtender>
    </p>
        <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblpmobile" runat="server" Text="Personal Mobile Number" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtmobileno" runat="server" Width="300px" Height="25px"></asp:TextBox>
            <asp:FilteredTextBoxExtender ID="txtmobileno_FilteredTextBoxExtender" 
                runat="server" Enabled="True" TargetControlID="txtmobileno" 
                ValidChars="0123456789">
            </asp:FilteredTextBoxExtender>
            <asp:RoundedCornersExtender ID="txtmobileno_RoundedCornersExtender" 
                runat="server" Enabled="True" TargetControlID="txtmobileno">
            </asp:RoundedCornersExtender>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblhobbies" runat="server" Text="Hobbies" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txthobbies" runat="server" Width="300px" Height="25px"></asp:TextBox>
        <asp:RoundedCornersExtender ID="txthobbies_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="txthobbies">
        </asp:RoundedCornersExtender>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lbllikes" runat="server" Text="Likes" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtlikes" runat="server" Width="300px" Height="25px"></asp:TextBox>
        <asp:RoundedCornersExtender ID="txtlikes_RoundedCornersExtender" runat="server" 
            Enabled="True" TargetControlID="txtlikes">
        </asp:RoundedCornersExtender>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lbldislikes" runat="server" Text="Dislikes" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtdislikes" runat="server" Width="300px" Height="25px"></asp:TextBox>
        <asp:RoundedCornersExtender ID="txtdislikes_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="txtdislikes">
        </asp:RoundedCornersExtender>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblabtu" runat="server" Text="About You" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtaboutu" runat="server" Width="300px" Height="25px" TextMode="MultiLine"></asp:TextBox>
        <asp:DropDownExtender ID="txtaboutu_DropDownExtender" runat="server" 
            DynamicServicePath="" Enabled="True" TargetControlID="txtaboutu">
        </asp:DropDownExtender>
        <asp:RoundedCornersExtender ID="txtaboutu_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="txtaboutu">
        </asp:RoundedCornersExtender>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnpinfoupdate" runat="server" Text="Update" Width="200px" 
            BackColor="Black" BorderColor="#CC0000" BorderStyle="Ridge" BorderWidth="5px" 
            ForeColor="White" Height="40px" onclick="btnpinfoupdate_Click"/>
        <asp:RoundedCornersExtender ID="btnpinfoupdate_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="btnpinfoupdate">
        </asp:RoundedCornersExtender>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnpinfocancel" runat="server" Text="Cancel" Width="200px" 
            BackColor="Black" BorderColor="#CC0000" BorderStyle="Ridge" BorderWidth="5px" 
            ForeColor="White" Height="40px" onclick="btnpinfocancel_Click"/>
        <asp:RoundedCornersExtender ID="btnpinfocancel_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="btnpinfocancel">
        </asp:RoundedCornersExtender>
     </p>
    <p>
    </p>
    <p>
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lbleducation" runat="server" Text="Education" 
            ForeColor="White" Width="300px" BorderColor="White" BorderStyle="Double" 
            BorderWidth="5px" Height="30px" Font-Bold="True" Font-Size="X-Large"></asp:Label>
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblcurrent" runat="server" Text="Current Status" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:RadioButton ID="rdbstudent" runat="server" ForeColor="White" 
            GroupName="edu" Text="Student"/>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:RadioButton ID="rdbworking" runat="server" ForeColor="White" 
            GroupName="edu" Text="Working"/>
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblhighschool" runat="server" Text="High School" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txthighschool" runat="server" Width="300px" Height="25px"></asp:TextBox>
        <asp:RoundedCornersExtender ID="txthighschool_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="txthighschool">
        </asp:RoundedCornersExtender>
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblinterclg" runat="server" Text="Inter College" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtinterclg" runat="server" Width="300px" Height="25px"></asp:TextBox>
        <asp:RoundedCornersExtender ID="txtinterclg_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="txtinterclg">
        </asp:RoundedCornersExtender>
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblgraduation" runat="server" Text="Graduation" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtgraduation" runat="server" Width="300px" Height="25px"></asp:TextBox>
        <asp:RoundedCornersExtender ID="txtgraduation_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="txtgraduation">
        </asp:RoundedCornersExtender>
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblpostgrad" runat="server" Text="Post Graduation" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtpostgrad" runat="server" Width="300px" Height="25px"></asp:TextBox>
        <asp:RoundedCornersExtender ID="txtpostgrad_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="txtpostgrad">
        </asp:RoundedCornersExtender>
    </p>
    <p>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btneduupdate" runat="server" Text="Update" Width="200px" 
            BackColor="Black" BorderColor="#CC0000" BorderStyle="Ridge" BorderWidth="5px" 
            ForeColor="White" Height="40px" onclick="btneduupdate_Click"/>
        <asp:RoundedCornersExtender ID="btneduupdate_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="btneduupdate">
        </asp:RoundedCornersExtender>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btneducancel" runat="server" Text="Cancel" Width="200px" 
            BackColor="Black" BorderColor="#CC0000" BorderStyle="Ridge" BorderWidth="5px" 
            ForeColor="White" Height="40px" onclick="btneducancel_Click"/>
        <asp:RoundedCornersExtender ID="btneducancel_RoundedCornersExtender" 
            runat="server" Enabled="True" TargetControlID="btneducancel">
        </asp:RoundedCornersExtender>
    </p>
    <p>
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblwrkinfo" runat="server" Text="Work Information" 
            ForeColor="White" Width="300px" BorderColor="White" BorderStyle="Double" 
            BorderWidth="5px" Height="30px" Font-Bold="True" Font-Size="X-Large"></asp:Label>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblprofession" runat="server" Text="Profession" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtprofession" runat="server" Width="300px" Height="25px"></asp:TextBox>
        <asp:RoundedCornersExtender ID="RoundedCornersExtender1" 
            runat="server" Enabled="True" TargetControlID="txtprofession">
        </asp:RoundedCornersExtender>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblcname" runat="server" Text="Company Name" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtcname" runat="server" Width="300px" Height="25px"></asp:TextBox>
        <asp:RoundedCornersExtender ID="RoundedCornersExtender2" 
            runat="server" Enabled="True" TargetControlID="txtcname"></asp:RoundedCornersExtender>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblcadd" runat="server" Text="Company Address" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtcadd" runat="server" Width="300px" TextMode="MultiLine" 
            Height="40px"></asp:TextBox>
        <asp:DropDownExtender ID="DropDownExtender1" runat="server" 
         DynamicServicePath="" Enabled="True" TargetControlID="txtcadd">
     </asp:DropDownExtender>
        <asp:RoundedCornersExtender ID="RoundedCornersExtender3" 
            runat="server" Enabled="True" TargetControlID="txtcadd"></asp:RoundedCornersExtender>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblcompanymobile" runat="server" Text="Office Mobile Number" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtcmobile" runat="server" Width="300px" Height="25px"></asp:TextBox>
            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                runat="server" Enabled="True" TargetControlID="txtcmobile" 
                ValidChars="0123456789">
            </asp:FilteredTextBoxExtender>
            <asp:RoundedCornersExtender ID="RoundedCornersExtender4" 
                runat="server" Enabled="True" TargetControlID="txtcmobile">
            </asp:RoundedCornersExtender>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblcemail" runat="server" Text="Company Email ID" ForeColor="White" Width="200px"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtcemail" runat="server" Width="300px" Height="25px"></asp:TextBox>
        <asp:RoundedCornersExtender ID="RoundedCornersExtender5" 
            runat="server" Enabled="True" TargetControlID="txtcemail">
        </asp:RoundedCornersExtender>
    </p>
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnwinfoupdate" runat="server" Text="Update" Width="200px" 
            BackColor="Black" BorderColor="#CC0000" BorderStyle="Ridge" BorderWidth="5px" 
            ForeColor="White" Height="40px" onclick="btnwinfoupdate_Click"/>
        <asp:RoundedCornersExtender ID="RoundedCornersExtender6" 
            runat="server" Enabled="True" TargetControlID="btnwinfoupdate">
        </asp:RoundedCornersExtender>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnwinfocancel" runat="server" Text="Cancel" Width="200px" 
            BackColor="Black" BorderColor="#CC0000" BorderStyle="Ridge" BorderWidth="5px" 
            ForeColor="White" Height="40px" onclick="btnwinfocancel_Click"/>
        <asp:RoundedCornersExtender ID="RoundedCornersExtender7" 
            runat="server" Enabled="True" TargetControlID="btnwinfocancel">
        </asp:RoundedCornersExtender>
    </p>
</asp:Content>

