﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class userlogged_updateprofile : System.Web.UI.Page
{
    SqlConnection cn;
    SqlCommand cmd;
    SqlDataReader rd;
    protected void Page_Load(object sender, EventArgs e)
    {
        cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Y510\Documents\Visual Studio 2010\WebSites\NASS\App_Data\user_profile.mdf;Integrated Security=True;User Instance=True;MultipleActiveResultSets=True");
        try
        {
            int i = Convert.ToInt32(Session["userid"]);
            string username = Session["username"].ToString();
            int j = Convert.ToInt32(Session["i"]);
            if (j == 1)
            {
                hplfirstname.Text = Session["firstname"].ToString() + " " + Session["lastname"].ToString();
            }
            if (j == 2)
            {
                hplfirstname.Text = Session["displayname"].ToString();
            }
            imgprofile.ImageUrl = Session["profileimage"].ToString();
        }
        catch
        {
            Response.Redirect("~//login//login.aspx");
        }
        string selectpersonalinfo = "Select * from personalinfo where (userid='" + Session["userid"].ToString() + "')";
        string selecteducation = "Select * from education where (userid='" + Session["userid"].ToString() + "')";
        string selectworkinfo = "Select * from workinfo where (userid='" + Session["userid"].ToString() + "')";
        if (!IsPostBack)
        {
            cmd = new SqlCommand(selectpersonalinfo, cn);
            cn.Open();
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                txtfirstname.Text = rd["firstname"].ToString();
                txtlastname.Text = rd["lastname"].ToString();
                if (rd["displayname"].ToString() == null||rd["displayname"].ToString() =="")
                {
                    txtdisplayname.Text = rd["firstname"].ToString() + " " + rd["lastname"].ToString();
                }
                else
                {
                    txtdisplayname.Text = rd["displayname"].ToString();
                }
                txtdob.Text = rd["dateofbirth"].ToString();
                txtpadd.Text = rd["personaladdress"].ToString();
                txtmobileno.Text = rd["personalmobileno"].ToString();
                txthobbies.Text = rd["hobbies"].ToString();
                txtlikes.Text = rd["likes"].ToString();
                txtdislikes.Text = rd["dislikes"].ToString();
                txtaboutu.Text = rd["aboutu"].ToString();
            }
            rd.Close();
            cmd = new SqlCommand(selecteducation, cn);
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                int i = 0;
                ViewState["i"] = i.ToString();
                if (rd["currentstatus"].ToString() == "Student")
                {
                    rdbstudent.Checked = true;
                    i++;
                    ViewState["i"] = i.ToString();
                }
                if (rd["currentstatus"].ToString() == "Working")
                {
                    i++;
                    rdbworking.Checked = true;
                    ViewState["i"] = i.ToString();
                }
                i = Convert.ToInt32(ViewState["i"]);
                if (i == 0)
                {
                    rdbstudent.Checked = false;
                    rdbworking.Checked = false;
                }
                txthighschool.Text = rd["highschool"].ToString();
                txtinterclg.Text = rd["intercollege"].ToString();
                txtgraduation.Text = rd["graduation"].ToString();
                txtpostgrad.Text = rd["postgraduation"].ToString();
            }
            rd.Close();
            cmd = new SqlCommand(selectworkinfo, cn);
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                txtprofession.Text = rd["profession"].ToString();
                txtcname.Text = rd["companyname"].ToString();
                txtcadd.Text = rd["companyaddress"].ToString();
                txtcmobile.Text = rd["officemob"].ToString();
                txtcemail.Text = rd["companyemail"].ToString();
            }
        }
        cn.Close();
    }
    protected void btnwinfocancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~//userlogged//updateprofile.aspx");
    }
    protected void btneducancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~//userlogged//updateprofile.aspx");        
    }
    protected void btnpinfocancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~//userlogged//updateprofile.aspx");
    }

    protected void btnpinfoupdate_Click(object sender, EventArgs e)
    {
        string updatepi = "update personalinfo set firstname='" + txtfirstname.Text + "',lastname='" + txtlastname.Text + "',displayname='" + txtdisplayname.Text + "',dateofbirth='" + txtdob.Text + "',personaladdress='" + txtpadd.Text + "',personalmobileno='" + txtmobileno.Text + "',hobbies='" + txthobbies.Text + "',likes='" + txtlikes.Text + "',dislikes='" + txtdislikes.Text + "',aboutu='" + txtaboutu.Text + "' where userid='" + Session["userid"].ToString() + "'";
        cmd = new SqlCommand(updatepi, cn);
        cn.Open();
        cmd.ExecuteNonQuery();
        cn.Close();
        string select = "select * from personalinfo where userid='" + Session["userid"].ToString() + "'";
        cmd = new SqlCommand(select, cn);
        cn.Open();
        rd = cmd.ExecuteReader();
        while (rd.Read())
        {
            if (rd["displayname"].ToString() == "")
            {
                break;
            }
            else
            {
                Session["i"] = 2;
                Session["displayname"] = rd["displayname"].ToString();
            }
        }
        rd.Read();
        cn.Close();
        Response.Redirect("~//userlogged//updateprofile.aspx");
    }
    protected void btneduupdate_Click(object sender, EventArgs e)
    {
        if (rdbstudent.Checked == true)
        {
            string updateedu = "update education set currentstatus='" + rdbstudent.Text + "',highschool='" + txthighschool.Text + "',intercollege='" + txtinterclg.Text + "',graduation='" + txtgraduation.Text + "',postgraduation='" + txtpostgrad.Text + "' where userid='" + Session["userid"].ToString() + "'";
            cmd = new SqlCommand(updateedu, cn);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
            Response.Redirect("~//userlogged//updateprofile.aspx");
        }
        if (rdbworking.Checked == true)
        {
            string updateedu2 = "update education set currentstatus='" + rdbworking.Text + "',highschool='" + txthighschool.Text + "',intercollege='" + txtinterclg.Text + "',graduation='" + txtgraduation.Text + "',postgraduation='" + txtpostgrad.Text + "' where userid='" + Session["userid"].ToString() + "'";
            cmd = new SqlCommand(updateedu2, cn);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
            Response.Redirect("~//userlogged//updateprofile.aspx");
        }
    }
    protected void btnwinfoupdate_Click(object sender, EventArgs e)
    {
        string updatewinfo = "update workinfo set profession='" + txtprofession.Text + "',companyname='" + txtcname.Text + "',companyaddress='" + txtcadd.Text + "',officemob='" + txtcmobile.Text + "',companyemail='" + txtcemail.Text + "' where userid='" + Session["userid"].ToString() + "'";
        cmd = new SqlCommand(updatewinfo, cn);
        cn.Open();
        cmd.ExecuteNonQuery();
        cn.Close();
        Response.Redirect("~//userlogged//updateprofile.aspx");
    }
}