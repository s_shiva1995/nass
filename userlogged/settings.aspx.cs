﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class userlogged_settings : System.Web.UI.Page
{
    SqlConnection cn;
    SqlCommand cmd,cmd2;
    SqlDataReader rd;
    protected void Page_Load(object sender, EventArgs e)
    {
        cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Y510\Documents\Visual Studio 2010\WebSites\NASS\App_Data\user_profile.mdf;Integrated Security=True;User Instance=True;MultipleActiveResultSets=True;");
        try
        {
            int i = Convert.ToInt32(Session["userid"]);
            string username = Session["username"].ToString();
            int j = Convert.ToInt32(Session["i"]);
            if (j == 1)
            {
                hplfirstname.Text = Session["firstname"].ToString() + " " + Session["lastname"].ToString();
            }
            if (j == 2)
            {
                hplfirstname.Text = Session["displayname"].ToString();
            }
            imgprofile.ImageUrl = Session["profileimage"].ToString();
        }
        catch
        {
            Response.Redirect("~//login//login.aspx");
        }
    }
    protected void btnuname_Click(object sender, EventArgs e)
    {
        string select="select * from detail where userid='"+Session["userid"]+"'";
        string updateusername="update detail set username='"+txtnewuname.Text+"' where userid='"+Session["userid"]+"'";
        cmd = new SqlCommand(select, cn);
        cn.Open();
        rd = cmd.ExecuteReader();
        while (rd.Read())
        {
            if (txtolduname.Text == rd["username"].ToString())
            {
                cmd2 = new SqlCommand(updateusername, cn);
                cmd2.ExecuteNonQuery();
            }
        }
        rd.Close();
        cn.Close();
    }
    protected void btnpassword_Click(object sender, EventArgs e)
    {
        string select = "select * from detail where userid='" + Session["userid"] + "'";
        string updatepassword = "update detail set password='" + txtnewpass.Text + "' where userid='" + Session["userid"] + "'";
        cmd = new SqlCommand(select, cn);
        cn.Open();
        rd = cmd.ExecuteReader();
        while (rd.Read())
        {
            if (txtoldpass.Text == rd["password"].ToString())
            {
                cmd2 = new SqlCommand(updatepassword, cn);
                cmd2.ExecuteNonQuery();
            }
        }
        rd.Close();
        cn.Close();
    }
    protected void btnemail_Click(object sender, EventArgs e)
    {
        string select = "select * from detail where userid='" + Session["userid"] + "'";
        string updateemail = "update detail set email='" + txtnewemail.Text + "' where userid='" + Session["userid"] + "'";
        cmd = new SqlCommand(select, cn);
        cn.Open();
        rd = cmd.ExecuteReader();
        while (rd.Read())
        {
            if (txtoldemail.Text == rd["email"].ToString())
            {
                cmd2 = new SqlCommand(updateemail, cn);
                cmd2.ExecuteNonQuery();
            }
        }
        rd.Close();
        cn.Close();
    }
}