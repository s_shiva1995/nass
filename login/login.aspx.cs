﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class login_login : System.Web.UI.Page
{
    SqlConnection cn;
    SqlCommand cmd,cmd1,cmd2;
    SqlDataReader rd,rd1,rd2;
    protected void Page_Load(object sender, EventArgs e)
    {
        cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Y510\Documents\Visual Studio 2010\WebSites\NASS\App_Data\user_profile.mdf;Integrated Security=True;User Instance=True;MultipleActiveResultSets=True");

    }
    protected void btnlogin_Click(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        int check=0;
        ViewState["check"] = check.ToString();
        string select = "select * from detail";
        cmd = new SqlCommand(select, cn);
        cn.Open();
        rd = cmd.ExecuteReader();
        while (rd.Read())
        {
            if (txtusername.Text == rd["username"].ToString())
            {
                check++;
                ViewState["check"] = check.ToString();
                string select2="select * from detail where (username='"+txtusername.Text+"')";
                cmd1 = new SqlCommand(select2, cn);
                rd1 = cmd1.ExecuteReader();
                while (rd1.Read())
                {
                    ViewState["profileimage"] = rd1["profileimage"].ToString();
                    ViewState["firstname"] = rd1["firstname"].ToString();
                    ViewState["lastname"] = rd1["lastname"].ToString();                    
                    ViewState["userid"] = rd1["userid"];
                    ViewState["password"] = rd1["password"].ToString();
                    string pass = ViewState["password"].ToString();
                    if (pass == txtpassword.Text)
                    {
                        string select3 = "select * from personalinfo where userid='" +ViewState["userid"].ToString()+"'";
                        cmd2 = new SqlCommand(select3, cn);
                        rd2=cmd2.ExecuteReader();
                        while (rd2.Read())
                        {
                            int p;
                            if (rd2["displayname"].ToString() == "" || rd2["displayname"].ToString()==null )
                            {
                                Session["firstname"]=ViewState["firstname"].ToString();
                                Session["lastname"]=ViewState["lastname"].ToString();
                                p = 1;
                                Session["i"] = p.ToString();
                            }
                            else
                            {
                                Session["displayname"] = rd2["displayname"].ToString();
                                p = 2;
                                Session["i"] = p.ToString();
                            }
                        } rd2.Close();
                        Session["userid"] = ViewState["userid"].ToString();
                        Session["profileimage"] = ViewState["profileimage"].ToString();
                        Session["username"] = txtusername.Text;
                        txtusername.Text = "";
                        txtpassword.Text = "";
                        lblmsg.Text = "Login Successful";
                        Response.Redirect("~//userlogged//logged1.aspx");
                    }
                    else
                    {
                        txtpassword.Text = "";
                        lblmsg.Text = "Username and Password doesn't match";
                    }
                }
                rd1.Close();
                
            }
        }
        rd.Close();
        int i=Convert.ToInt32(ViewState["check"]);
        if (i == 0)
        {
            txtusername.Text = "";
            txtpassword.Text = "";
            lblmsg.Text = "Invalid Username";
        }
        cn.Close();
     }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~//welcome//home.aspx");
    }
}
