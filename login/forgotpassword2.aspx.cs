﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class login_forgotpassword2 : System.Web.UI.Page
{
    SqlConnection cn;
    SqlCommand cmd;
    SqlDataReader rd;
    protected void Page_Load(object sender, EventArgs e)
    {
        cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Y510\Documents\Visual Studio 2010\WebSites\NASS\App_Data\user_profile.mdf;Integrated Security=True;User Instance=True;MultipleActiveResultSets=True;");
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        int check = 0;
        string select = "select * from detail";
        cmd = new SqlCommand(select, cn);
        cn.Open();
        rd = cmd.ExecuteReader();
        while (rd.Read())
        {
            if (txtusername.Text == rd["username"].ToString())
            {
                Session["username"] = txtusername.Text;
                check++;
                Session["securityquestion"] = rd["securityquestion"].ToString();
                Response.Redirect("~//login//forgotpassword.aspx");
                break;
            }
        } rd.Close();
        cn.Close();
        if (check == 0)
        {
            lblmsg.Text = "Enter Valid Username";
        }
    }
}