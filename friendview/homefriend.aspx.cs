﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class friendview_homefriend : System.Web.UI.Page
{
    SqlCommand cmd;
    SqlDataReader rd;
    SqlConnection cn;
    protected void Page_Load(object sender, EventArgs e)
    {
        cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Y510\Documents\Visual Studio 2010\WebSites\NASS\App_Data\user_profile.mdf;Integrated Security=True;User Instance=True;MultipleActiveResultSets=True");
        string select = "select * from detail where username='" + Session["friendusername"].ToString() + "'";
        cmd = new SqlCommand(select, cn);
        cn.Open();
        rd = cmd.ExecuteReader();
        while (rd.Read())
        {
            imgprofile.ImageUrl = rd["profileimage"].ToString();
            hplfirstname.Text = rd["firstname"].ToString() + " " + rd["lastname"].ToString();
            Session["useridfriend"] = rd["userid"].ToString();
        }
        rd.Close();
        cn.Close();
        string friendselect = "select * from friend where (userid2='" + Session["useridfriend"] + "' and userid1='" + Session["userid"] + "') OR (userid1='" + Session["useridfriend"] + "' and userid2='" + Session["userid"] + "')";
            cmd = new SqlCommand(friendselect, cn);
            cn.Open();
            rd = cmd.ExecuteReader();
            if (rd.Read())
            {
                if (Convert.ToInt32(rd["friendstat"]) == 1)
                {
                    if (rd["userid1"].ToString() == Session["userid"].ToString())
                    {
                        btnfriend.Text = "Cancel Friend Request";
                    }
                    if (rd["userid2"].ToString() == Session["userid"].ToString())
                    {
                        btnfriend.Text = "Confirm Friend";
                    }
                }
                if (Convert.ToInt32(rd["friendstat"]) == 2)
                {
                    btnfriend.Text = "Remove Friend";
                }
            }
            else
            {
                btnfriend.Text = "Add Friend";
            }

            rd.Close();
            cn.Close();
    }

    protected void btnfriend_Click(object sender, EventArgs e)
    {
        if (btnfriend.Text == "Add Friend")
        {
            string insert = "insert into friend values ('" + Session["userid"] + "','" + Session["useridfriend"] + "','1')";
            cmd = new SqlCommand(insert, cn);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
            string insert2 = "insert into friend values ('" + Session["useridfriend"] + "','" + Session["userid"] + "','1')";
            cmd = new SqlCommand(insert2, cn);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
            btnfriend.Text = "Cancel Friend Request";
            Response.Redirect("~//friendview//homefriend.aspx");
        }
        if (btnfriend.Text == "Cancel Friend Request")
        {
            string delete = "delete from friend where userid2='" + Session["useridfriend"] + "' and userid1='" + Session["userid"] + "'";
            cmd = new SqlCommand(delete, cn);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
            string delete2 = "delete from friend where userid2='" + Session["userid"] + "' and userid1='" + Session["useridfriend"] + "'";
            cmd = new SqlCommand(delete2, cn);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
            btnfriend.Text = "Add Friend";
            Response.Redirect("~//friendview//homefriend.aspx");
        }
        if (btnfriend.Text == "Remove Friend")
        {
            string delete = "delete from friend where userid2='" + Session["useridfriend"] + "' and userid1='" + Session["userid"] + "'";
            cmd = new SqlCommand(delete, cn);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
            string delete2 = "delete from friend where userid2='" + Session["userid"] + "' and userid1='" + Session["useridfriend"] + "'";
            cmd = new SqlCommand(delete2, cn);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
            btnfriend.Text = "Add Friend";
            Response.Redirect("~//friendview//homefriend.aspx");
        }
        if (btnfriend.Text == "Confirm Friend")
        {
            string update = "update friend set friendstat='2' where userid2='" + Session["userid"] + "' and userid1='" + Session["useridfriend"] + "'";
            cmd = new SqlCommand(update, cn);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
            string update2 = "update friend set friendstat='2' where userid2='" + Session["useridfriend"] + "' and userid1='" + Session["userid"] + "'";
            cmd = new SqlCommand(update2, cn);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
            btnfriend.Text = "Remove Friend";
            Response.Redirect("~//friendview//homefriend.aspx");
        }
    }
}